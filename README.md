# Порт перевода The Jackbox Party Pack 9 от What If? на Nintendo Switch

### Как установить этот перевод?
Шаг 1. [Склонируйте данный репозиторий.](https://codeberg.org/JB-RU-NX/TJPP9-WI/archive/main.zip)\
Шаг 2. Скопируйте папку `atmosphere` из папки `TJPP9-WI` находящийся в архиве `TJPP9-WI-main.zip` в корень вашей SD карты.\
Шаг 3. Запустить The Jackbox Party Pack 9 и наслаждать :)